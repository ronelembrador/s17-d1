// const array1 = ['eat', 'sleep'];
// console.log(array1);

// const array2 = new Array('pray', 'play');
// console.log(array2);


// empty array
// const myList = [];

// array of numbers
// const numArray = [2, 3, 4, 5];

// array of strings
// let stringArray = ['eat', 'work', 'pray', 'play'];

// array of mixed
// let newData = ['work', 1, true];

// let newData1 = [
// 	{'task1': 'exercise'},
// 	[1,2,3],
// 	function hello(){
// 		console.log('Hi I am array.')
// 	}
// ];
// console.log(newData1);

/*
	Mini-Activity

	Create an array with 7 items; all strings.
		- list seven of the places you want to visit someday
	Log the first item in the console
	Log all the items in the console.
	*/
	let placesToVisit = [
	'Tokyo Tower', 
	'Great Wall of China',
	'Empire State Building',
	'Disneyland',
	'Hollywood',
	'Tower of London',
	'Eiffel Tower'
	];
// console.log(placesToVisit[0]);
// console.log(placesToVisit);
// console.log(placesToVisit[placesToVisit.length-1]);
// console.log(placesToVisit.length);

// for(let i = 0; i < placesToVisit.length; i++){
// 	console.log(placesToVisit[i]);
// };

// Array manipulation
// Add element to an array - push() add element at the end of the array
let dailyActivities = ['eat', 'work', 'pray', 'play'];
dailyActivities.push('exercise');
console.log(dailyActivities);
// unshift() add element at the beginning of the array
dailyActivities.unshift('sleep');
console.log(dailyActivities);

dailyActivities[2] = 'sing';
console.log(dailyActivities);

dailyActivities[6] = 'dance';
console.log(dailyActivities);

// Re-assign the values/items in an array:
placesToVisit[3] = 'Giza Sphinx';
console.log(placesToVisit);
console.log(placesToVisit[5]);

placesToVisit[5] = 'Turkey';
console.log(placesToVisit);
console.log(placesToVisit[5]);
/*
	Re-assign the values for the first and last item in the array.
		-Re-assign it with your hometown and your highschool
	log the array in the console
	Log the first and last items in the console.

	*/
	placesToVisit[0] = 'San Pedro';
	placesToVisit[placesToVisit.length-1] = 'SPRCNHS';
	console.log(placesToVisit);
	console.log(placesToVisit[0]);
	console.log(placesToVisit[placesToVisit.length-1]);

// Adding items in an array without using methods:
let array = [];
console.log(array[0]);
array[0] = 'Cloud Strife';
console.log(array);
console.log(array[1]);
array[1] = 'Tifa Lockhart';
console.log(array[1]);
console.log(array);
array[array.length-1] = 'Aerith Gainsborough';
console.log(array);
array[array.length] = 'Vincent Valentine';
console.log(array);

// Array Methods
	// Manipulate array with pre-determined JS Functions
	// Mutators - these arrays methods usually change the original array
	
	let array1 = ['Juan', 'Pedro', 'Jose', 'Andres'];
	// without method
	array1[array.length] = 'Francisco';
	console.log(array1);

	// .push() - allows us to add an element at the end of the array
	array1.push('Andres');
	console.log(array1);

	// .unshift() - allows us to add an element at the beginning of the array
	array1.unshift('Simon');
	console.log(array1);

	// .pop() - allows us to delete or remove the last item/element at the end of the array
	array1.pop();
	console.log(array1);
	// .pop() is also able to return the item we removed.
	console.log(array1.pop());
	console.log(array1);

	let removedItem = array1.pop();
	console.log(array1);
	console.log(removedItem);

	// .shift() return the item we removed
	let removedItemShift = array1.shift();
	console.log(array1);
	console.log(removedItemShift);


	// Mini Activity
	array1.shift();
	console.log(array1);

	array1.pop();
	console.log(array1);

	array1.unshift('George');
	console.log(array1);

	array1.push('Michael');
	console.log(array1);


// array1.shift();
// console.log(array1);
// array1.pop();
// console.log(array1);
// array1.unshift('George');
// console.log(array1);
// array1.push('Michael');

// .sort() - by default, will allow us to sort our items in ascending order.
// array1.sort();
// console.log(array1);

// let numArray = [3, 2, 1, 6, 7, 9];
// numArray.sort();
// console.log(numArray);	

// let numArray2 = [32, 400, 450, 2, 9, 5, 50, 90];
// numArray2.sort((a,b)=> a-b);
// console.log(numArray2);
// .sort() converts all items into strings and then arrange the items accordinly as if they are words/text

// ascending sort per number's value
// numArray2.sort(function(a,b){
// 	return a-b
// })
// console.log(numArray2);

// descending sort
// numArray2.sort(function(a,b){
// 	return b-a
// })
// console.log(numArray2)

// let arrayStr = ['Marie', 'Zen', 'Jamie', 'Elaine'];
// arrayStr.sort(function(a,b){
// 	return b - a
// })
// console.log(arrayStr);

// .reverse() - reversed the order of the items
// arrayStr.sort();
// console.log(arrayStr);
// arrayStr.sort().reverse();
// console.log(arrayStr);


// let beatles = ['George', 'John', 'Paul', 'Ringo'];
// let lakersPlayers = ['Lebron', 'Davis', 'Westbrook', 'Kobe', 'Shaq'];
// splice() - allows us to remove and add elements from a given index. 
// syntax: array.splice(startingIndex, numberofItemstobeDeleted, elementstoAdd)
// lakersPlayers.splice(0, 0, 'Caruso');
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 1);
// console.log(lakersPlayers);
// lakersPlayers.splice(0, 3);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,1);
// console.log(lakersPlayers);
// lakersPlayers.splice(1,0, 'Gasol', 'Fisher');
// console.log(lakersPlayers);

// Non-Mutators
	// Methods that will not change the original
	// slice() - allows us to get a portion of the original array and return a new array with the items selected from the original
	// syntax: slice(startIndex, endIndex)
	
// let computerBrands = ['IBM', 'HP', 'Apple', 'MSI'];
// computerBrands.splice(2, 2, 'Compaq', 'Toshiba', 'Acer');
// console.log(computerBrands);

// let newBrands = computerBrands.slice(1, 3);
// console.log(computerBrands);
// console.log(newBrands);

let fonts = ['Times New Roman', 'Comic San MS', 'Impact', 'Monotype Corsiva', 'Arial', 'Arial Black'];
console.log(fonts);

let newFontSet = fonts.slice(1, 5);
console.log(newFontSet);

newFontSet = fonts.slice().reverse();
console.log(newFontSet);

/*
	Mini-Activity
	Given a set of data, use slice to copy the last two items in the array.
	Save the sliced portion of the array into a new variable: 
		- microsoft
	use slice to copy the third and fourth item in the array.
	Save the sliced portion of the array into a new variable: 
		- nintendo
	log both new arrays in the console.
	*/
	let videoGame = ['PS4', 'PS5', 'Switch', 'Xbox', 'Xbox1']

	let microsoft = videoGame.slice(3);
	let nintendo = videoGame.slice(2,4);

	console.log(microsoft);
	console.log(nintendo);

// .toSTring() - convert the array into a single value as a string but each item will be separated by a comma
// syntax: array.toString()

let sentence = ['I','like','JavaScript','.','It','is','fun','.'];
let sentenceString = sentence.toString();
console.log(sentence);
console.log(sentenceString);

// .join() - converts the array into a single value as a string but separator can be specified
// syntax: array.join(separator)

let sentence2 = ['My', 'Favorite', 'fastfood', 'is', 'Army Navy'];
let sentenceString2 = sentence2.join(' ');
console.log(sentenceString2);
let sentenceString3 = sentence2.join('/');
console.log(sentenceString3)

/* 			
			Mini-Activity

			Given a set of characters, 
				-form the name, "Martin" as a single string.
				-form the name, "Miguel" as a single string.
			Use the methods we discussed so far.

			save "Martin" and "Miguel" to variables:
				-name 1 and name 2

			log both variables on the console.

			*/


			let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];

			let name1char = charArr.slice(5,11);
			let name1 = name1char.join('');

			let name2char = charArr.slice(13,19);
			let name2 = name2char.join('');

			console.log(name1);
			console.log(name2);

// .concat() - it combines 2 or more array without affecting the original
// syntax: array.concat(array1, array2)

let tasksFriday = ['drink HTML', 'eat JavaScript'];
let tasksSaturday = ['inhale CSS', 'breath Bootstrap'];
let tasksSunday = ['Get Git', 'Be Node'];

let weekendTasks = tasksFriday.concat(tasksSaturday, tasksSunday);

// Accessors
	// Methods that allow us to access our array.
	// indexOf()
	// 	- finds the index of the given element/item when it is first found from the left

	let batch131 = ['Paolo', 'Jamir', 'Jed', 'Ronel', 'Rom', 'Jayson'];

console.log(batch131.indexOf('Jed')); //2
console.log(batch131.indexOf('Rom')); //4

// lastIndexOf()
	//	- finds the index of the given element/item when it is last found from the right

console.log(batch131.lastIndexOf('Jamir')); //1
console.log(batch131.lastIndexOf('Jayson')); //5

console.log(batch131.lastIndexOf('Kim')); //-1

/*
	Mini-Activity
	Given a set of brands with some entries repeated:
		Create a function which can display the index of the brand that was input the first time it was found in the array.

		Create a function which can display the index of the brand that was input the last time it was found in the array.
		*/
		
let carBrands = [
'BMW',
'Dodge',
'Maserati',
'Porsche',
'Chevrolet',
'Ferrari',
'GMC',
'Porsche',
'Mitsubhisi',
'Toyota',
'Volkswagen',
'BMW'
];

function firstIndex(brand){
	return carBrands.indexOf(brand);
}

function lastIndex(brand2){
	return carBrands.lastIndexOf(brand2);
}


let BMWIndex1 = firstIndex('BMW');
let BMWIndex2 = lastIndex('BMW');
let BMWIndexes = console.log(`The index where BMW can first be found is ${BMWIndex1} and last be found is ${BMWIndex2}`);

let PorscheIndex1 = firstIndex('Porsche');
let PorscheIndex2 = lastIndex('Porsche');
let PorscheIndexes = console.log(`The index where Porsche can first be found is ${PorscheIndex1} and last be found is ${PorscheIndex2}`);


// Iterator Methods
	// These methods iterate over the itemns in an array much like loop
	// However, with our iterator methods there also that allows to not only iterate over items but also additional instruction.

let avengers = [
		'Hulk',
		'Black Widow',
		'Hawkeye',
		'Spider-man',
		'Iron Man',
		'Captain America'
];

// forEach() 
	//similar to for loop but is used on arrays. It will allow us to iterate over each time in an array and even add instruction per iteration
//Anonymous function within forEach will be receive each and evey item in an array

avengers.forEach(function(avenger){
	console.log(avenger);
});


let marvelHereos = [
		'Moon Knight',
		'Jessica Jones',
		'Deadpool',
		'Cyclops'
];

marvelHereos.forEach(function(hero){
	// iterate over all the items in Marvel Heroes array and let them join the avengers
	if(hero !== 'Cyclops' && hero !== 'Deadpool'){
		// Add an if-else wherein Cyclops and Deadpool is not allow to join
		avengers.push(hero);
	}
});
console.log(avengers);

// map() - similar to forEach however it returns new array

let number = [25, 50, 30, 10, 5];

let mappedNumbers = number.map(function(number){
	console.log(number);
	return number * 5
});
console.log(mappedNumbers);

// every()
	// iterates ovel all the items and checks if all the elements passess a given condition

let allMemberAge = [25, 30, 15, 20, 26];

let checkAllAdult = allMemberAge.every(function(age){
	console.log(age);
	return age >= 18
});

console.log(checkAllAdult);
// some()
	// iterate over all the items check if even at least one of items in the array passes the condition. Same as every(), it will return boolean
let examScores = [75, 80, 74, 71];
let checkForPassing = examScores.some(function(score){
	console.log(score);
	return score >= 80;
});
console.log(checkForPassing);

// filter() - creates a new array that contains elements which passed a given condition

let numbersArr2 = [500, 12, 120, 60, 6, 30];

let divisibleBy5 = numbersArr2.filter(function(number){
	return number % 5 === 0;
});
console.log(divisibleBy5);

// find() - iterate over all items in our array but only returns the item that will satisfy the given condition

let registeredUsername = ['pedro101', 'mikeyTheKing2000', 'superPhoenix', 'sheWhoCode'];
	
let foundUser = registeredUsername.find(function(username){
	console.log(username);
	return username === 'mikeyTheKing2000';
})
console.log(foundUser);

// .includes() - returns a boolean true if it finds a matching item in the array.
// Case-sensitive

let registeredEmails = [
	'johnnyPhoenix1991@gmail.com',
	'michaelKing@gmail.com',
	'pedro_himself@yahoo.com',
	'sheJonesSmith@gmail.com'
]

let doesEmailExist = registeredEmails.includes('michaelKing@gmail.com')

console.log(doesEmailExist)


/*
	Mini-Activity
	Create 2 functions
		first function is able to find specified or the username input in our registeredUsernames array.
		Display the result in the console

		Second Function is able to find a specified email already existing in the registeredEmails array.
			- If there is an email found, show an alert: 'Email Already Exists'
			- If there is no email found. show an alert: 'Email is available, proceed to registration'

		You may use any of the three methods we discussed recently.

*/

let registeredUsernames = ['jackSon', 'BobbyLee', 'johnSon', 'jackieLou'];

function findUsername(username){
	return registeredUsernames.includes(username);
}

console.log(findUsername('BobbyLee'));

function findEmail(email){
	return registeredEmails.includes(email);
}

let emailRegister = findEmail('bobbylee1989@gmail.com');

if (emailRegister === true){
	alert('Email Already Exists');
} else {
	alert('Email is available, proceed to registration');
}